# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=klickety
pkgver=23.08.0
pkgrel=0
pkgdesc="An adaptation of the Clickomania game"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/games/klickety/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/games/klickety.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/klickety-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4aa9b85a52a9fee9e315f849fcbc87dc619f10621440c7d4e1ba867d674a7d35d77495bbb102782afff8909cd1e24baa71a5e8288589bd036c76469927ff6f6c  klickety-23.08.0.tar.xz
"
