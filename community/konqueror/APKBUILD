# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=konqueror
pkgver=23.08.0
pkgrel=0
pkgdesc="KDE File Manager & Web Browser"
# armhf blocked by extra-cmake-modules
# s390x, ppc64le and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !s390x !ppc64le !riscv64"
url="https://konqueror.org/"
license="GPL-2.0-or-later AND GFDL-1.2-only AND LicenseRef-KDE-Accepted-LGPL"
makedepends="
	extra-cmake-modules
	kactivities-dev
	karchive-dev
	kcmutils-dev
	kcrash-dev
	kdbusaddons-dev
	kdelibs4support-dev
	kdesu-dev
	kdoctools-dev
	kiconthemes-dev
	kinit-dev
	kparts-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/network/konqueror.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/konqueror-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Requires OpenGL and running D-Bus server

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
96d1f188dadf58adc8d6fdf44ee352f9280fb88abd825ddf12ce25213a98283d5a264eec2e2ab5b190b593ab9aed73093f8ef8fa157be137339ae67e79221643  konqueror-23.08.0.tar.xz
"
