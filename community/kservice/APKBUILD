# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kservice
pkgver=5.109.0
pkgrel=0
pkgdesc="Advanced plugin and service introspection"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later"
depends_dev="
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	ki18n-dev
	"
makedepends="$depends_dev
	bison
	doxygen
	extra-cmake-modules
	flex-dev
	kdoctools-dev
	qt5-qttools-dev
	samurai
	"
_repo_url="https://invent.kde.org/frameworks/kservice.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kservice-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kservice.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# ksycoca_xdgdirstest, kmimeassociationstest kapplicationtradertest and ksycocathreadtest are broken
	# ksycocatest requires running X
	local skipped_tests="("
	local tests="
		kapplicationtrader
		kmimeassociations
		ksycoca
		ksycoca_xdgdirs
		ksyscocathread
		"
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)test"
	ctest --test-dir build --output-on-failure -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
99f6c07214274f721cefc51d84f09a329ea01bb526e4ed3e7d89313312e2c9edc043b1c1f55aba4a91c224e1334a8e6a6f29d5fdb3101ad0dcf698fe0d9fcfac  kservice-5.109.0.tar.xz
"
