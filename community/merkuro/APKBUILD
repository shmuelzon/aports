# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=merkuro
pkgver=23.08.0
pkgrel=0
pkgdesc="A calendar application using Akonadi to sync with external services (NextCloud, GMail, ...)"
# armhf blocked by qt5-qtdeclarative
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://invent.kde.org/pim/kalendar"
license="GPL-3.0-or-later AND BSD-2-Clause"
depends="
	kdepim-runtime
	kirigami-addons
	kirigami2
	qt5-qtlocation
	"
makedepends="
	akonadi-contacts-dev
	akonadi-dev
	eventviews-dev
	extra-cmake-modules
	kcalendarcore-dev
	kconfigwidgets-dev
	kcontacts-dev
	kcoreaddons-dev
	ki18n-dev
	kirigami-addons-dev
	kirigami2-dev
	kitemmodels-dev
	kpackage-dev
	kpeople-dev
	kwindowsystem-dev
	mailcommon-dev
	qqc2-desktop-style-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtlocation-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/pim/merkuro.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/merkuro-$pkgver.tar.xz"
options="!check" # No tests

provides="kalendar=$pkgver-r$pkgrel"
replaces="kalendar"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
441b6aedb39207707b3257b6e9768f46bf4cfc7e0ac7f7e309625755c3ba2b2c449044ce16491a1032150311ece99d88b179c060cc2904bdafbc894e5b2d66c0  merkuro-23.08.0.tar.xz
"
