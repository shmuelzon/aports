# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=filelight
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/filelight"
pkgdesc="An application to visualize the disk usage on your computer"
license="(GPL-2.0-only OR GPL-3.0-only) AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/utilities/filelight.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/filelight-$pkgver.tar.xz
	0001-Fix-musl-build.patch
	"
subpackages="$pkgname-dbg $pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ab2b86e3d479a4a07eb531dfdaaa63f4a981dfa255b2e79748de8dc0aa8aad51dff5054fbe93d4c6e79a4f5b82fe0e6d9c1224d052685bc2b499995adc426616  filelight-23.08.0.tar.xz
db0d2fe06a3073c981814989edbaa97cd8b14fa05894c735273b30fc3f95c69207db22bd87db31a703d8f187d1b65827663d60192ba979b977ad51d4cdaa37ad  0001-Fix-musl-build.patch
"
