# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=akonadi-calendar
pkgver=23.08.0
pkgrel=0
pkgdesc="Akonadi calendar integration"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by kmailtransport -> libkgapi -> qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later"
depends_dev="
	akonadi-contacts-dev>=$pkgver
	akonadi-dev>=$pkgver
	kcalendarcore-dev
	kcalutils-dev
	kcodecs-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kidentitymanagement-dev
	kio-dev
	kmailtransport-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	messagelib-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/akonadi-calendar.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-calendar-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # Broken

replaces="kalendar>1.0.0"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
e12132764c5042a2e0a07ca7b90e6701e1915eb6fb1e513da969b982894beba590a76e3c27007a7f43dd542fd8cb162a4c67bf7df7ba143cd1b3601befb0a9fc  akonadi-calendar-23.08.0.tar.xz
"
