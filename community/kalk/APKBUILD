# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kalk
pkgver=23.08.0
pkgrel=0
pkgdesc="A powerful cross-platfrom calculator application"
arch="all !armhf" # Blocked by qt5-qtdeclarative
url="https://invent.kde.org/plasma-mobile/kalk"
license="GPL-3.0-or-later"
depends="qt5-qtfeedback"
makedepends="
	bison
	extra-cmake-modules
	flex
	gmp-dev
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kirigami2-dev
	kunitconversion-dev
	mpfr-dev
	qt5-qtbase-dev
	qt5-qtfeedback-dev
	qt5-qtquickcontrols2-dev
	samurai
	"
_repo_url="https://invent.kde.org/utilities/kalk.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kalk-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

check() {
	cd build

	# inputmanagertest_de is broken, https://invent.kde.org/plasma-mobile/kalk/-/issues/25
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "inputmanagertest_de|knumbertest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
057eaa6015a4e6194cb4de626cb54a0a1210f3dd35928c627bb1e238fedb418f40c7097de23b8fac6bb739bf0cf96ffaf3a8fdfdc56a0bc5d3e1420e127a241c  kalk-23.08.0.tar.xz
"
