# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=tokodon
pkgver=23.08.0
pkgrel=0
pkgdesc="A Mastodon client for Plasma and Plasma Mobile"
url="https://invent.kde.org/network/tokodon/"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="GPL-3.0-only AND CC0-1.0"
depends="
	kirigami-addons
	kirigami2
	"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kio-dev
	kirigami-addons-dev
	kirigami2-dev
	knotifications-dev
	mpv-dev
	qqc2-desktop-style-dev
	qt5-qtbase-dev
	qt5-qtmultimedia-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	qt5-qtwebsockets-dev
	qtkeychain-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/network/tokodon.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/tokodon-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2527f04b3766a9182df04c3b27efc0edb9508e06c75f2b21056e39defb21597f53d849384630bc1612df422255509939632decaf996c7aa874f4cec2f727e31b  tokodon-23.08.0.tar.xz
"
