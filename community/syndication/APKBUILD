# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=syndication
pkgver=5.109.0
pkgrel=0
pkgdesc="An RSS/Atom parser library"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later AND BSD-3-Clause"
depends_dev="
	kcodecs-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	doxygen
	graphviz
	qt5-qttools-dev
	samurai
	"
_repo_url="https://invent.kde.org/frameworks/syndication.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/syndication-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/syndication.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1ba85da1d36382dfd80e0de3ad3e35cecac2fa4d75e96f6d25199cb171c55a0a88e606509b4629f1a9d7d1cdd4151909ad7f2b339c39d83a5077cbdc8861eee4  syndication-5.109.0.tar.xz
"
