# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kdenetwork-filesharing
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/applications/internet/"
pkgdesc="Properties dialog plugin to share a directory with the local network"
license="GPL-2.0-only OR GPL-3.0-only"
depends="samba"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kcoreaddons-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	qcoro-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/network/kdenetwork-filesharing.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdenetwork-filesharing-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DSAMBA_INSTALL=OFF
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c5eb97e1df674054eaa2be172ad0fca136cc91002739d912e928b1449e6ee6fc895e6a430e2c0f5cba755bface4e4771b2d476955f20af2d1605c1102e1c0c1f  kdenetwork-filesharing-23.08.0.tar.xz
"
