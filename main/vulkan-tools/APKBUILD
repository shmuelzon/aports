# Contributor: Bart Ribbers <bribbers@disroot.org>
# Contributor: Simon Zeni <simon@bl4ckb0ne.ca>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=vulkan-tools
_pkgname=Vulkan-Tools
pkgver=1.3.261.0
pkgrel=0
arch="all"
url="https://www.khronos.org/vulkan"
pkgdesc="Vulkan Utilities and Tools"
license="Apache-2.0"
makedepends="
	cmake
	glslang-dev
	libx11-dev
	libxrandr-dev
	python3
	samurai
	vulkan-headers
	vulkan-loader-dev
	wayland-dev
	wayland-protocols-dev
	"
source="https://github.com/KhronosGroup/Vulkan-Tools/archive/refs/tags/sdk-$pkgver/vulkan-tools-sdk-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-sdk-$pkgver"
options="!check" # No tests

build() {
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_INSTALL_SYSCONFDIR=/etc \
		-DCMAKE_INSTALL_DATADIR=/usr/share \
		-DCMAKE_SKIP_RPATH=True \
		-DBUILD_CUBE=ON \
		-DBUILD_VULKANINFO=ON \
		-DGLSLANG_INSTALL_DIR=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8bc30ead031ad6bceb7e54a1b70f26048847a30147d86638ced9d621b11ef64b1e97f41394b068458f5c8ad6e30216aba6e14765e971fa9e6a49773a47593fe5  vulkan-tools-sdk-1.3.261.0.tar.gz
"
