# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=trivy
pkgver=0.45.0
pkgrel=0
pkgdesc="Simple and comprehensive vulnerability scanner for containers"
url="https://github.com/aquasecurity/trivy"
arch="all"
# s390x: tests SIGSEGV: https://github.com/aquasecurity/trivy/issues/430
arch="$arch !s390x"
license="Apache-2.0"
makedepends="btrfs-progs-dev go linux-headers lvm2-dev"
source="https://github.com/aquasecurity/trivy/archive/v$pkgver/trivy-$pkgver.tar.gz"
options="net !check" # needs tinygo to turn go into wasm for tests

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	export CGO_ENABLED=0

	go build -o trivy -ldflags "-X main.version=$pkgver" cmd/trivy/main.go
}

check() {
	go test ./...
}

package() {
	install -Dm755 trivy -t "$pkgdir"/usr/bin/
}

sha512sums="
6fd4c0177b4921b14ca43f2f04183b4ad7b30554f2a03602ff1af28aeb77ffc630ded695bebe23cb4549fe66e9c48a3b646e0058ecc27f1f74d917eb0d96d33c  trivy-0.45.0.tar.gz
"
