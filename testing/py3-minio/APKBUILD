# Maintainer: Cowington Post <cowingtonpost@gmail.com>
pkgname=py3-minio
pkgver=7.1.16
pkgrel=0
pkgdesc="MinIO client SDK for Python"
url="https://docs.min.io/docs/python-client-quickstart-guide.html"
arch="noarch"
license="Apache-2.0"
depends="py3-certifi py3-urllib3"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/minio/minio-py/archive/$pkgver/py3-minio-$pkgver.tar.gz"
builddir="$srcdir/minio-py-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
f6754edc305f87e7c28c16f7f6c4380dac9ce9e7cb342cf2d0294ee0aa862ebbd7ee34bec779b8e1717cffa1290cb591713cea54c864647fc2c3f8e5f6e64153  py3-minio-7.1.16.tar.gz
"
